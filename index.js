const express = require("express");
const axios = require("axios");
const app = express();
const pool = require("./db");
const https = require("https");
const fs = require("fs");
const path = require("path");
const redis = require("redis");
const responseTime = require("response-time");
const {promisify} = require("util");

app.use(express.json()) // req.body
//app.use(responseTime());

const client = redis.createClient({
    host: '127.0.0.1',
    port: 6379,
})

/*
const GET_ASYNC = promisify(client.get).bind(client)
const SET_ASYNC = promisify(client.set).bind(client)
*/

//GET ALL
app.get("/todos", async (req, res) => {
    try{
        const allTodos = await pool.query("SELECT * FROM gradovi");

        res.json({
            "@context": {         
                "description": "https://schema.org/Thing",
                "population": "https://schema.org/GeoCoordinates"
            }, 
            status: "OK",
            message: "Fetched object",
            response: allTodos.rows,
            links: [
                {
                    href: "id\todos",
                    rel: "id",
                    type: "GET, POST"
                }
            ]
        });
        res.json(allTodos.rows);
    } catch (err) {
        res.status(404).json({
            status: "404 Not found",
            message: "Doesn't exist",
            response: null
        })
    }
})

//GET
app.get("/todos/:id", async (req, res) => {
    const { id } = req.params;
    try {
        const todo = await pool.query("select * FROM gradovi WHERE todo_id = $1", [id]);
        let grad = todo.rows[0].city;
        //console.log(grad)
        res.json({
            "@context": {         
                "description": "https://schema.org/Thing",
                "population": "https://schema.org/GeoCoordinates"
            },  
            status: "OK",
            message: "Fetched object",
            response: [
                todo.rows[0]
            ],
            links: [
                {
                    href: "id\todos",
                    rel: "self",
                    type: "GET, DELETE, PUT"
                }
            ]
        });
        
    } catch (err) {
        res.status(404).json({
            status: "404 Not found",
            message: "Doesn't exist",
            response: null
        })
    }
});

//POST
app.post("/todos", async(req, res) => {
    try{
        const { description, city, country, wikipedia, districts, mayor, timezone0timezonename, timezone1timezonename, area_km2, density_km2, areacode, population, slika, pathToPicture } = req.body;
        const newTodo = await pool.query(
        "INSERT INTO gradovi (description, city, country, wikipedia, districts, mayor, timezone0timezonename, timezone1timezonename, area_km2, density_km2, areacode, population, slika, pathToPicture) VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13, $14) RETURNING *",
            [description , city, country, wikipedia, districts, mayor, timezone0timezonename, timezone1timezonename, area_km2, density_km2, areacode, population, slika, pathToPicture]
        );
        
        res.json({
            status: "OK",
            message: "Fetched object",
            response: [newTodo.rows[0]],
            links: [
                {
                    href: "todos",
                    rel: "self",
                    type: "POST, GET"
                }
            ]
        });
        
    } catch (err) {
        res.status(404).json({
            status: "404 Not found",
            message: "Doesn't exist",
            response: null
        })
    }
});

//PUT
app.put("/todos/:id", async (req, res) => {
    try {
        const { id } = req.params; //WHERE
        if (req.body.description) {
            const { description } = req.body; //SET

            const updateTodo = await pool.query("UPDATE gradovi SET description = $1 WHERE todo_id = $2",
            [description, id]
            );
        } 
        if (req.body.city) {
            const { city } = req.body; //SET

            const updateTodo = await pool.query("UPDATE gradovi SET city = $1 WHERE todo_id = $2",
            [city, id]
            );
        }
        if (req.body.country) {
            const { country } = req.body; //SET

            const updateTodo = await pool.query("UPDATE gradovi SET country = $1 WHERE todo_id = $2",
            [country, id]
            );
        }
        if (req.body.wikipedia) {
            const { wikipedia } = req.body; //SET

            const updateTodo = await pool.query("UPDATE gradovi SET wikipedia = $1 WHERE todo_id = $2",
            [wikipedia, id]
            );
        }
        if (req.body.districts) {
            const { districts } = req.body; //SET

            const updateTodo = await pool.query("UPDATE gradovi SET districts = $1 WHERE todo_id = $2",
            [districts, id]
            );
        }
        if (req.body.mayor) {
            const { mayor } = req.body; //SET

            const updateTodo = await pool.query("UPDATE gradovi SET mayor = $1 WHERE todo_id = $2",
            [mayor, id]
            );
        }
        if (req.body.timezone0timezonename) {
            const { timezone0timezonename } = req.body; //SET

            const updateTodo = await pool.query("UPDATE gradovi SET timezone0timezonename = $1 WHERE todo_id = $2",
            [timezone0timezonename, id]
            );
        }
        if (req.body.timezone1timezonename) {
            const { timezone1timezonename } = req.body; //SET

            const updateTodo = await pool.query("UPDATE gradovi SET timezone1timezonename = $1 WHERE todo_id = $2",
            [timezone1timezonename, id]
            );
        }
        if (req.body.area_km2) {
            const { area_km2 } = req.body; //SET

            const updateTodo = await pool.query("UPDATE gradovi SET area_km2 = $1 WHERE todo_id = $2",
            [area_km2, id]
            );
        }
        if (req.body.density_km2) {
            const { density_km2 } = req.body; //SET

            const updateTodo = await pool.query("UPDATE gradovi SET density_km2 = $1 WHERE todo_id = $2",
            [density_km2, id]
            );
        }
        if (req.body.areacode) {
            const { areacode } = req.body; //SET

            const updateTodo = await pool.query("UPDATE gradovi SET areacode = $1 WHERE todo_id = $2",
            [areacode, id]
            );
        }
        if (req.body.population) {
            const { population } = req.body; //SET

            const updateTodo = await pool.query("UPDATE gradovi SET population = $1 WHERE todo_id = $2",
            [population, id]
            );
        }
        if (req.body.slika) {
            const { slika } = req.body; //SET

            const updateTodo = await pool.query("UPDATE gradovi SET slika = $1 WHERE todo_id = $2",
            [slika, id]
            );
        }
        if (req.body.pathToPicture) {
            const { pathToPicture } = req.body; //SET

            const updateTodo = await pool.query("UPDATE gradovi SET pathToPicture = $1 WHERE todo_id = $2",
            [pathToPicture, id]
            );
        }
        
        res.json({
            status: "OK",
            message: "Updated",
            response: [req.body],
            links: [
                {
                    href: "id\todos",
                    rel: "self",
                    type: "PUT, DELETE, GET"
                }
            ]
        });
        
    } catch (err) {
        res.status(404).json({
            status: "404 Not found",
            message: "Doesn't exist",
            response: null
        })
    }
});

//DELETE
app.delete("/todos/:id", async (req, res) => {
    try {
        const { id } = req.params;
        const deleteTodo = await pool.query("DELETE FROM gradovi WHERE todo_id = $1", 
        [id]
        );
        
        res.json({
            status: "OK",
            message: "Deleted",
            response: "",
            links: [
                {
                    href: "id\todos",
                    rel: "self",
                    type: "DELETE, GET, PUT"
                }
            ]
        });
    } catch (err) {
        res.status(404).json({
            status: "404 Not found",
            message: "Doesn't exist",
            response: null
        })
    }
});

app.listen(5000, () => {
    console.log("server is listening on port 5000");
});





app.patch("/todos/:id", async (req, res) => {
    try {
        var a = 3;
        throw(a == 3);
    } catch (err) {
        res.status(501).json({
            status: "501 Not Implemented",
            message: "Not Implemented",
            response: null
        })
    }

})


//GET PICTURE
app.get("/todos/:id/picture", async (req, res) => {
    
    try {
        const { id } = req.params
        const todo = await pool.query("select * FROM gradovi WHERE todo_id = $1", [id])
        let grad = todo.rows[0].city;

        let searchUrl = "https://en.wikipedia.org/api/rest_v1/page/summary/"
        const response = await axios.get(searchUrl + grad)
        let srcUri = response.data.originalimage.source
        
        const filename = path.basename(srcUri)
        /*const reply = await GET_ASYNC('picture') 
        if (reply) {
            console.log('using cached data')
            res.send(JSON.parse(reply));
            return;
        }*/
        https.get(srcUri, async function(res2) {
            try {
                //const saveResult = await SET_ASYNC('picture', JSON.stringify(response), 'EX', 10)
                //console.log('new data cached', saveResult)
                const fileStream = fs.createWriteStream(filename);
                res2.pipe(fileStream);

                fileStream.on("error", function(err)
                {
                    console.log(err);
                })    

                fileStream.on("finish", function(){
                    fileStream.close();
                })
                //client.setex(id, 5, )

            } catch (error) {
                res.send(error.message)
                
            }
        })
        const postoji = __dirname + '/' + filename;
        if (postoji) {
            res.sendFile(__dirname + '/' + filename);
        } else {
            res.send(__dirname + '/' + filename);
        }
        //res.send(__dirname + '/' + filename);
        //res.sendFile(__dirname + '/' + filename);

    } catch (error) {
        res.send(error.message)
    }
});
