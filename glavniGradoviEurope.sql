--
-- PostgreSQL database dump
--

-- Dumped from database version 13.0
-- Dumped by pg_dump version 13.0

-- Started on 2020-10-27 03:00:20

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- TOC entry 200 (class 1259 OID 16815)
-- Name: gradovi; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.gradovi (
    city character varying(9) NOT NULL,
    country character varying(11) NOT NULL,
    wikipedia character varying(9) NOT NULL,
    districts integer NOT NULL,
    mayor character varying(18) NOT NULL,
    timezone0timezonename character varying(3) NOT NULL,
    timezone1timezonename character varying(4) NOT NULL,
    area_km2 integer NOT NULL,
    density_km2 integer NOT NULL,
    areacode integer NOT NULL,
    population integer NOT NULL
);


ALTER TABLE public.gradovi OWNER TO postgres;

--
-- TOC entry 2979 (class 0 OID 16815)
-- Dependencies: 200
-- Data for Name: gradovi; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.gradovi (city, country, wikipedia, districts, mayor, timezone0timezonename, timezone1timezonename, area_km2, density_km2, areacode, population) FROM stdin;
Zagreb	Croatia	Zagreb	17	Milan Bandić	CET	CEST	641	4055	3851	790017
London	England	London	33	Sadiq Khan Labour	GMT	BST	1572	5666	2	8961989
Vienna	Austria	Vienna	1	Michael Ludwig	CET	CEST	419	4326	14301	1867582
Paris	France	Paris	20	Anne Hidalgo	CET	CEST	105	20000	33	2148271
Berlin	Germany	Berlin	12	Michael Muller	CET	CEST	891	4230	30	3769495
Rome	Italy	Rome	8	Virginia Raggi	CET	CEST	1285	2236	6	4342212
Athens	Greece	Athens	7	Kostas Bakoyannis	EET	EEST	38964	17	21	664046
Amsterdam	Netherlands	Amsterdam	51	Femke Halsema	CET	CEST	219	5214	20	879680
Warsaw	Poland	Warsaw	18	Rafal Trzaskowski	CET	CEST	517	3469	4822	1790658
Stockholm	Sweden	Stockholm	11	Anna Konig Jerlmyr	CET	CEST	188	5200	468	975904
Budapest	Hungary	Budapest	3	Gergely Karacsony	CET	CEST	525	3388	1	1752286
\.


--
-- TOC entry 2848 (class 2606 OID 16819)
-- Name: gradovi gradovi_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.gradovi
    ADD CONSTRAINT gradovi_pkey PRIMARY KEY (city);


-- Completed on 2020-10-27 03:00:20

--
-- PostgreSQL database dump complete
--

